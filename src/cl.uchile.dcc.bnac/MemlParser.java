/*
 *  bnac : virtual museum environment creation/manipulation project
 *  Copyright (C) 2010 Felipe Cañas Sabat
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cl.uchile.dcc.bnac;

import java.io.IOException;

import java.util.LinkedList;

import javax.vecmath.Point2f;

import org.xml.sax.SAXException;
import org.xml.sax.Attributes;
import org.xml.sax.ext.DefaultHandler2;

public class MemlParser extends DefaultHandler2
{
    public MuseumEnvironment me;

    protected CObject obj;
    protected Capture cap;
    protected Hall hall;
    protected LinkedList<Point2f> corners;
    protected Route route;
    protected boolean inStrings;
    protected String strId;
    protected String strLang;

    protected String txt;

    public MemlParser (MuseumEnvironment me)
    {
        this.me = me;
        obj = null;
        cap = null;
        hall = null;
        corners = null;
        route = null;
        inStrings = false;
        strId = null;
        strLang = null;

        txt = null;
    }

    public void startElement (String uri, String lName, String qName,
            Attributes attrs) throws SAXException
    {
        if (attrs.getLength() > 0) {
            BnacLog.trace("MemlParser.startElement %s >>", qName);
            for (int i=0; i<attrs.getLength(); ++i) {
                BnacLog.trace("%s=%s",
                        attrs.getQName(i), attrs.getValue(i));
            }
            BnacLog.trace("<<");
        } else {
            BnacLog.trace("MemlParser.startElement %s %s %s >><<",
                    uri, lName, qName);
        }
        if (qName.equals("painting")) {
            obj = new CObject(attrs.getValue("id"));
        } else if (qName.equals("capture2D")) {
            cap = new Capture2D(attrs.getValue("id"),
                    me.getObject(attrs.getValue("ref")));
        } else if (qName.equals("hall")) {
            hall = new Hall(attrs.getValue("id"),
                    Float.parseFloat(attrs.getValue("height")),
                    new Point2f[0]);
            if (attrs.getValue("name") != null) {
                hall.setName(attrs.getValue("name"));
            }
            corners = new LinkedList<Point2f>();
        } else if (qName.equals("link")) {
            me.addLink(new Link(
                    attrs.getValue("id"),
                    attrs.getValue("door1"), attrs.getValue("door2")));
        } else if (qName.equals("route")) {
            route = new Route(attrs.getValue("id"));
        } else if (qName.equals("strings")) {
            inStrings = true;
        } else if (qName.equals("elem")) {
            strId = attrs.getValue("id");
        } else if (qName.equals("entry")) {
            strLang = attrs.getValue("lang");
        } else if (qName.equals("pair") && inStrings) {
            me.addString(strId, strLang, attrs.getValue("name"),
                    attrs.getValue("value"));
        } else {
            if (obj != null) {
            } else if (cap != null) {
                if (qName.equals("resource")) {
                    cap.set("Type", attrs.getValue("type"));
                }
            } else if (hall != null) {
                if (qName.equals("corner")) {
                    corners.add(new Point2f(
                                Float.parseFloat(attrs.getValue("x")),
                                Float.parseFloat(attrs.getValue("y"))));
                } else if (qName.equals("placedCapture")) {
                    if (corners != null) {
                        hall.setCorners(corners.toArray(new Point2f[0]));
                        corners = null;
                    }

                    PlacedCapture pc = new PlacedCapture(
                            attrs.getValue("id"),
                            me.getCapture(attrs.getValue("ref")),
                            new Point2f(
                                Float.parseFloat(attrs.getValue("x")),
                                Float.parseFloat(attrs.getValue("y"))),
                            0.0f, 1.0f);
                    if (attrs.getValue("angle") != null) {
                        pc.angle = (float) -Math.toRadians(
                                Float.parseFloat(attrs.getValue("angle")));
                    }
                    if (attrs.getValue("scale") != null) {
                        pc.scale = Float.parseFloat(attrs.getValue("scale"));
                    }
                    hall.getWall(Integer.parseInt(attrs.getValue("wall"))).
                        addPlacedCapture(pc);
                } else if (qName.equals("door")) {
                    if (corners != null) {
                        hall.setCorners(corners.toArray(new Point2f[0]));
                        corners = null;
                    }

                    String id = attrs.getValue("id");
                    Wall wall = hall.getWall(
                            Integer.parseInt(attrs.getValue("wall")));
                    Door d = new Door(id);
                    Point2f dims = d.getDimensions();
                    Point2f pos = d.pos;
                    if (attrs.getValue("w") != null) {
                        dims.x = Float.parseFloat(attrs.getValue("w"));
                    }
                    if (attrs.getValue("h") != null) {
                        dims.y = Float.parseFloat(attrs.getValue("h"));
                    }
                    if (attrs.getValue("x") != null) {
                        pos.x = Float.parseFloat(attrs.getValue("x"));
                    }
                    if (attrs.getValue("y") != null) {
                        pos.y = Float.parseFloat(attrs.getValue("y"));
                    }
                    if (attrs.getValue("angle") != null) {
                        d.setAngle((float) -Math.toRadians(
                                Float.parseFloat(attrs.getValue("angle"))));
                    }
                    if (attrs.getValue("scale") != null) {
                        d.setScale(Float.parseFloat(attrs.getValue("scale")));
                    }
                    d.setDimensions(dims.x, dims.y);
                    d.setPosition(pos.x, pos.y);
                    wall.addDoor(d);
                } else if (qName.equals("startingPoint")) {
                    if (corners != null) {
                        hall.setCorners(corners.toArray(new Point2f[0]));
                        corners = null;
                    }

                    hall.addStartingPoint(
                            attrs.getValue("id"),
                            new Point2f(
                                Float.parseFloat(attrs.getValue("x")),
                                Float.parseFloat(attrs.getValue("y"))),
                            Float.parseFloat(attrs.getValue("angle")));
                }
            }
        }
    }

    public void endElement (String uri, String lName, String qName)
        throws SAXException
    {
        if (qName.equals("painting")) {
            me.addObject(obj);
            obj = null;
        } else if (qName.equals("capture2D")) {
            me.addCapture(cap);
            cap = null;
        } else if (qName.equals("hall")) {
            me.addHall(hall);
            hall = null;
        } else if (qName.equals("route")) {
            me.addRoute(route);
            route = null;
        } else if (qName.equals("strings")) {
            inStrings = false;
        } else if (txt != null) {
            String key = Util.capitalize(qName);
            if (obj != null) {
                obj.set(key, txt);
            } else if (cap != null) {
                cap.set(key, txt);
            } else if (hall != null) {
                hall.set(key, txt);
            } else if (qName.equals("title")) {
                me.set("Title", txt);
            } else if (route != null && qName.equals("capture")) {
                route.addCapture(txt);
            }
        }
        txt = null;
    }

    public void characters (char[] ch, int idx, int len)
        throws SAXException
    {
        txt = new String(ch, idx, len);
    }
}
