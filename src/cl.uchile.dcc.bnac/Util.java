/*
 *  bnac : virtual museum environment creation/manipulation project
 *  Copyright (C) 2010 Felipe Cañas Sabat
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cl.uchile.dcc.bnac;

import javax.vecmath.Point2f;
import javax.vecmath.Vector3f;

public class Util
{
    final static public boolean isClockwise (Point2f[] ps)
    {
        Vector3f[] vs = new Vector3f[ps.length];
        for (int i=0; i<ps.length; ++i) {
            vs[i] = new Vector3f();
            vs[i].x = ps[(i+1)%ps.length].x - ps[i].x;
            vs[i].z = ps[(i+1)%ps.length].y - ps[i].y;
            vs[i].normalize();
        }

        int c = 0;
        Vector3f t = new Vector3f();
        for (int i=0; i<vs.length; ++i) {
            t.cross(vs[i], vs[(i+1)%vs.length]);
            c += (t.y > 0.00f) ? 1 : -1;
        }

        return (c > 0);
    }

    static public String capitalize (String s)
    {
        return s.substring(0, 1).toUpperCase().concat(s.substring(1));
    }

    private Util () { }
}

