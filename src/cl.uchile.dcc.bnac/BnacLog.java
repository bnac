/*
 *  bnac : virtual museum environment creation/manipulation project
 *  Copyright (C) 2010 Felipe Cañas Sabat
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cl.uchile.dcc.bnac;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BnacLog
{
    static protected Logger l;

    static {
        l = LoggerFactory.getLogger("cl.uchile.dcc.bnac");
    }

    static public void trace (String s) { l.trace(s); }
    static public void trace (String s, Object... args)
    {
        l.trace(String.format(s, args));
    }
    static public void debug (String s) { l.debug(s); }
    static public void debug (String s, Object... args)
    {
        l.debug(String.format(s, args));
    }
    static public void info (String s) { l.info(s); }
    static public void info (String s, Object... args)
    {
        l.info(String.format(s, args));
    }
    static public void warn (String s) { l.warn(s); }
    static public void warn (String s, Object... args)
    {
        l.warn(String.format(s, args));
    }
    static public void error (String s) { l.error(s); }
    static public void error (String s, Object... args)
    {
        l.error(String.format(s, args));
    }
    static public void fatal (String s) { fatal("FATAL %s", s); }
    static public void fatal (String s, Object... args)
    {
        l.error(String.format(s, args));
    }

    protected BnacLog () { }
}

