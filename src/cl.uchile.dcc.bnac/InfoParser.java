/*
 *  bnac : virtual museum environment creation/manipulation project
 *  Copyright (C) 2010 Felipe Cañas Sabat
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cl.uchile.dcc.bnac;

import java.io.IOException;

import java.util.LinkedList;

import javax.vecmath.Point2f;

import org.xml.sax.SAXException;
import org.xml.sax.Attributes;
import org.xml.sax.ext.DefaultHandler2;

public class InfoParser extends DefaultHandler2
{
    protected InfoSet info;
    protected InfoSet.InfoPage page;
    protected InfoSet.InfoEntry entry;
    protected boolean inTitle;
    protected boolean inDescr;

    public InfoParser (InfoSet info)
    {
        this.info = info;
        page = null;
        entry = null;
        inTitle = false;
        inDescr = false;
    }

    public void startElement (String uri, String lName, String qName,
            Attributes attrs) throws SAXException
    {
        if (qName.equals("page")) {
            page = info.makePage(attrs.getValue("id"));
            BnacLog.trace("new page %s", page.getId());
            if (attrs.getValue("next") != null) {
                page.setNext(attrs.getValue("next"));
                BnacLog.trace("%s -> %s", page.getId(), page.getNext());
            }
        } else if (qName.equals("image")) {
            page.setImage(attrs.getValue("src"));
            BnacLog.trace("image %s", page.getImage());
        } else if (qName.equals("entry")) {
            entry = page.makeEntry(attrs.getValue("lang"));
            BnacLog.trace("new entry %s", attrs.getValue("lang"));
            entry.title = "";
            entry.descr = "";
        } else if (qName.equals("title")) {
            inTitle = true;
        } else if (qName.equals("description")) {
            inDescr = true;
        }
    }

    public void endElement (String uri, String lName, String qName)
        throws SAXException
    {
        if (qName.equals("page")) {
            page = null;
        } else if (qName.equals("entry")) {
            entry = null;
        } else if (qName.equals("image")) {
        } else if (qName.equals("title")) {
            inTitle = false;
            BnacLog.trace("title %s", entry.title);
        } else if (qName.equals("description")) {
            inDescr = false;
            BnacLog.trace("descr%n%s", entry.descr);
        }
    }

    public void characters (char[] ch, int idx, int len)
        throws SAXException
    {
        if (inTitle) {
            StringBuffer sb = new StringBuffer(entry.title);
            sb.append(ch, idx, len);
            entry.title = new String(sb).trim();
        } else if (inDescr) {
            StringBuffer sb = new StringBuffer(entry.descr);
            sb.append(ch, idx, len);
            entry.descr = new String(sb).trim();
        }
    }
}

