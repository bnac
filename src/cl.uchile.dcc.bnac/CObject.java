/*
 *  bnac : virtual museum environment creation/manipulation project
 *  Copyright (C) 2010 Felipe Cañas Sabat
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cl.uchile.dcc.bnac;

import java.util.Properties;

public class CObject
{
    protected Properties props;

    public CObject (String id)
    {
        props = new Properties();

        props.setProperty(Attribute.Id.name(), id);
    }

    public void set (Attribute prop, String val)
    {
        props.setProperty(prop.name(), val);
    }
    public String get (Attribute prop)
    {
        return props.getProperty(prop.name());
    }

    public void set (String prop, String val)
    {
        props.setProperty(prop, val);
    }
    public String get (String prop)
    {
        return props.getProperty(prop);
    }

    public String getId () { return get(Attribute.Id.name()); }
}

