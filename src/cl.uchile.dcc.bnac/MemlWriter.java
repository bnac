/*
 *  bnac : virtual museum environment creation/manipulation project
 *  Copyright (C) 2010 Felipe Cañas Sabat
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cl.uchile.dcc.bnac;

import java.util.Map;
import java.util.Hashtable;
import java.io.File;
import java.io.PrintWriter;
import java.io.IOException;
import java.util.Locale;
import javax.vecmath.Point2f;

public class MemlWriter
{
    static public void write (MuseumEnvironment me, String path)
        throws IOException
    {
        File file = new File(path);
        file.createNewFile();
        write(me, file);
    }

    static public void write (MuseumEnvironment me, File file)
        throws IOException
    {
        Locale l = new Locale("en");
        PrintWriter fout = new PrintWriter(file);

        fout.printf(l, "<?xml version=\"1.0\" encoding=\"utf-8\"?>%n");
        fout.printf(l, "<meml>%n");
        fout.printf(l, "<title>%s</title>%n", me.getTitle());

        fout.printf(l, "%n");

        for (CObject obj: me.objectArray()) {
            fout.printf(l, "<painting id=\"%s\">%n", obj.get(Attribute.Id));
            fout.printf(l, "<author>%s</author>%n",
                    obj.get(Attribute.Author));
            fout.printf(l, "<title>%s</title>%n", obj.get(Attribute.Title));
            fout.printf(l, "<type>%s</type>%n", obj.get(Attribute.Type));
            fout.printf(l, "<width>%s</width>%n", obj.get(Attribute.Width));
            fout.printf(l, "<height>%s</height>%n",
                    obj.get(Attribute.Height));
            fout.printf(l, "</painting>%n");
        }

        fout.printf(l, "%n");

        for (Capture cap: me.captureArray()) {
            fout.printf(l, "<capture2D id=\"%s\" ref=\"%s\">%n",
                    cap.get(Attribute.Id),
                    cap.getReferencedObject().get(Attribute.Id));
            fout.printf(l, "<author>%s</author>%n",
                    cap.get(Attribute.Author));
            fout.printf(l, "<date>%s</date>%n", cap.get(Attribute.Date));
            fout.printf(l, "<resource type=\"%s\">%s</resource>%n",
                    cap.get(Attribute.Type), cap.get(Attribute.Resource));
            fout.printf(l, "<preview type=\"image/jpg\">%s</preview>%n",
                    cap.get(Attribute.Resource));
            fout.printf(l, "</capture2D>%n");
        }

        fout.printf(l, "%n");

        for (Hall hall: me.hallArray()) {
            Wall[] walls = hall.wallArray();

            fout.printf(l, "<hall id=\"%s\" height=\"%.2f\" name=\"%s\">%n",
                    hall.getId(), hall.getHeight(), hall.getName());
            for (Point2f p2f: hall.cornerArray()) {
                fout.printf(l, "<corner x=\"%.2f\" y=\"%.2f\"/>%n"
                        , p2f.x, p2f.y);
            }

            fout.printf(l, "%n");

            for (int i=0; i<walls.length; ++i) {
                for (PlacedCapture pcap: walls[i].placedCaptureArray()) {
                    fout.printf(l, 
                            "<placedCapture id=\"%s\" ref=\"%s\" " +
                            "wall=\"%d\" x=\"%.02f\" y=\"%.02f\" " +
                            "angle=\"%.02f\" scale=\"%.02f\"/>%n",
                            pcap.getId(), pcap.capture.get(Attribute.Id),
                            i, pcap.t.x, pcap.t.y,
                            Math.toDegrees(pcap.angle), pcap.scale);
                }
            }

            fout.printf(l, "%n");

            for (int i=0; i<walls.length; ++i) {
                for (Door door: walls[i].doorArray()) {
                    fout.printf(l, 
                            "<door id=\"%s\" wall=\"%d\" " +
                            "w=\"%.02f\" h=\"%.02f\" " +
                            "x=\"%.02f\" y=\"%.02f\" " +
                            "angle=\"%.02f\" scale=\"%.02f\"/>%n",
                            door.getId(), i,
                            door.getDimensions().x, door.getDimensions().y,
                            door.getPosition().x, door.getPosition().y,
                            Math.toDegrees(door.getAngle()), door.getScale());
                }
            }

            for (Hall.StartingPoint sp: hall.startingPointArray()) {
                fout.printf(l, 
                        "<startingPoint id=\"%s\" " +
                        "x=\"%.02f\" y=\"%.02f\" angle=\"%.02f\"/>%n",
                        sp.id, sp.pos.x, sp.pos.y, sp.angle);
            }

        fout.printf(l, "</hall>%n");
        }

        fout.printf(l, "%n");

        for (Link li: me.linkArray()) {
            fout.printf(l, "<link id=\"%s\" door1=\"%s\" door2=\"%s\"/>%n",
                    li.getId(), li.d1, li.d2);
        }

        fout.printf(l, "%n");

        for (Route r: me.routeArray()) {
            fout.printf(l, "<route id=\"%s\">%n", r.getId());
            for (String s: r.captureArray()) {
                fout.printf(l, "<capture>%s</capture>%n", s);
            }
            fout.printf(l, "</route>%n");
        }

        fout.printf(l, "%n");

        fout.printf("<strings>%n");
        for (Map.Entry<String, Hashtable<String,
                Hashtable<String, String> > > m: me.getStrings().entrySet()) {
            fout.printf("<elem id=\"%s\">%n", m.getKey());
            for (Map.Entry<String, Hashtable<String, String> > m2:
                    m.getValue().entrySet()) {
                fout.printf("<entry lang=\"%s\">%n", m2.getKey());
                for (Map.Entry<String, String> m3: m2.getValue().entrySet()) {
                    fout.printf("<pair name=\"%s\" value=\"%s\"/>%n",
                            m3.getKey(), m3.getValue());
                }
                fout.printf("</entry>%n");
            }
            fout.printf("</elem>%n");
        }
        fout.printf("</strings>%n");

        fout.printf(l, "</meml>%n");

        fout.flush();
        fout.close();
    }

    private MemlWriter () { }
}

