/*
 *  bnac : virtual museum environment creation/manipulation project
 *  Copyright (C) 2010 Felipe Cañas Sabat
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cl.uchile.dcc.bnac;

import java.util.ArrayList;
import java.util.Iterator;

import javax.vecmath.Point2f;
import javax.vecmath.Point3f;

public class Wall
{
    protected Point2f[] vs;

    protected ArrayList<Door> doors;
    protected ArrayList<PlacedCapture> pcs;

    public Wall (Point2f orig, Point2f end)
    {
        this(orig.x, orig.y, end.x-orig.x, end.y-orig.y);
    }
    public Wall (float x, float z, float dx, float dz)
    {
        BnacLog.debug("new Wall (%.02f,%.02f) +(%.02f,%.02f)",
                x, z, dx, dz);
        vs = new Point2f[2];
        vs[0] = new Point2f(x, z);
        vs[1] = new Point2f(vs[0]);
        vs[1].add(new Point2f(dx, dz));
        pcs = new ArrayList<PlacedCapture>();
        doors = new ArrayList<Door>();
    }

    public void setOrigin (Point2f p) { vs[0] = new Point2f(p); }
    public void setEnd (Point2f p) { vs[1] = new Point2f(p); }

    public Wall split (Point2f p)
    {
        BnacLog.debug("Wall.split (%.02f,%.02f)", p.x, p.y);
        Wall wal = new Wall(p, new Point2f(vs[1]));
        vs[1] = new Point2f(p);
        float d = vs[0].distance(vs[1]);

        Iterator<PlacedCapture> pcit = pcs.iterator();
        PlacedCapture pcap;
        Capture c;
        while (pcit.hasNext()) {
            pcap = pcit.next();
            if (pcap.t.x > d) {
                pcit.remove();
                wal.addPlacedCapture(pcap);
                pcap.t.x -= d;
            }
        }

        Iterator<Door> pcdr = doors.iterator();
        Door dr;
        while (pcdr.hasNext()) {
            dr = pcdr.next();
            if (dr.getPosition().x > d) {
                pcdr.remove();
                wal.addDoor(dr);
                dr.setPosition(dr.getPosition().x-d, dr.getPosition().y);
            }
        }

        return wal;
    }

    public Point2f origin2f () { return new Point2f(vs[0]); }
    public Point2f end2f () { return new Point2f(vs[1]); }
    public Point3f origin3f ()
    {
        return new Point3f(vs[0].x, 0.0f, -vs[0].y);
    }
    public Point3f end3f ()
    {
        return new Point3f(vs[1].x, 0.0f, -vs[1].y);
    }
    public Point2f[] getVertices ()
    {
        Point2f[] ret = new Point2f[2];
        ret[0] = vs[0];
        ret[1] = vs[1];
        return ret;
    }
    public float width () { return vs[0].distance(vs[1]); }

    public PlacedCapture getPlacedCapture (String id)
    {
        for (PlacedCapture pc: pcs) {
            if (pc.id.equals(id)) {
                return pc;
            }
        }
        return null;
    }
    public void addPlacedCapture (PlacedCapture pc)
    {
        BnacLog.debug("add PlacedCapture %s @(%.02f,%.02f) %.02f,%.02f",
                pc.getId(), pc.t.x, pc.t.y, pc.angle, pc.scale);
        pcs.add(pc);
        for (PlacedCapture pc2: pcs) { BnacLog.trace("pc: %s", pc2.getId()); }
    }
    public PlacedCapture removePlacedCapture (String id)
    {
        for (int i=0; i<pcs.size(); ++i) {
            if (pcs.get(i).getId().equals(id)) {
                BnacLog.debug("remove PlacedCapture %s", id);
                return pcs.remove(i);
            }
        }
        for (PlacedCapture pc: pcs) { BnacLog.trace("pc: %s", pc.getId()); }
        return null;
    }
    public PlacedCapture[] placedCaptureArray ()
    {
        return pcs.toArray(new PlacedCapture[0]);
    }

    public Door getDoor (String id)
    {
        for (Door d: doors) {
            if (d.getId().equals(id)) {
                return d;
            }
        }
        return null;
    }
    public void addDoor (Door d)
    {
        BnacLog.debug("add Door %s @(%.02f,%.02f)", d.getId(),
                d.getPosition().x, d.getPosition().y);
        doors.add(d);
    }
    public Door removeDoor (String id)
    {
        for (int i=0; i<doors.size(); ++i) {
            if (doors.get(i).getId().equals(id)) {
                BnacLog.debug("remove Door %s", id);
                return doors.remove(i);
            }
        }
        return null;
    }
    public Door[] doorArray () { return doors.toArray(new Door[0]); }
}

