/*
 *  bnac : virtual museum environment creation/manipulation project
 *  Copyright (C) 2010 Felipe Cañas Sabat
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cl.uchile.dcc.bnac;

import java.util.Set;
import java.util.Map;
import java.util.Hashtable;
import java.io.File;
import java.io.PrintWriter;
import java.io.IOException;
import java.util.Locale;
import javax.vecmath.Point2f;

public class InfoWriter
{
    static public void write (InfoSet info, String path)
        throws IOException
    {
        File file = new File(path);
        file.createNewFile();
        write(info, file);
    }

    static public void write (InfoSet info, File file)
        throws IOException
    {
        InfoSet.InfoEntry entry;
        String ind = "    ";
        Locale l = new Locale("en");
        PrintWriter fout = new PrintWriter(file);

        fout.printf(l, "<?xml version=\"1.0\" encoding=\"utf-8\"?>%n");
        fout.printf(l, "<info>%n");

        for (InfoSet.InfoPage page: info.pageArray()) {
            if (page.getNext() != null) {
                fout.printf(l, "%s<page id=\"%s\" next=\"%s\">%n",
                        ind, page.getId(), page.getNext());
            } else {
                fout.printf(l, "%s<page id=\"%s\">%n",
                        ind, page.getId());
            }

            if (page.getImage() != null) {
                fout.printf(l, "%s%s<image src=\"%s\"/>%n",
                        ind, ind, page.getImage());
            }

            for (Map.Entry<String, InfoSet.InfoEntry> ee: page.entrySet()) {
                entry = ee.getValue();
                fout.printf(l, "%s%s<entry lang=\"%s\">%n",
                        ind, ind, ee.getKey());

                fout.printf(l, "%s%s%s<title>%s</title>%n",
                        ind, ind, ind, entry.title);
                fout.printf(l,
                        "%s%s%s<description>%n%s%n%s%s%s</description>%n",
                        ind, ind, ind, entry.descr, ind, ind, ind);
                fout.printf(l, "%s%s</entry>%n", ind, ind);
            }

            fout.printf(l, "%s</page>%n", ind);
        }

        fout.printf(l, "</info>%n");

        fout.flush();
        fout.close();
    }
}

