/*
 *  bnac : virtual museum environment creation/manipulation project
 *  Copyright (C) 2010 Felipe Cañas Sabat
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cl.uchile.dcc.bnac;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Properties;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

public class MuseumEnvironment
{
    protected ArrayList<CObject> objects;
    protected ArrayList<Capture> captures;
    protected ArrayList<Hall> halls;
    protected ArrayList<Link> links;
    protected ArrayList<Route> routes;
    protected Hashtable<String, String> conns;
    protected Properties props;
    protected Hashtable<String, Hashtable<String,
           Hashtable<String, String> > > strings;

    public MuseumEnvironment ()
    {
        BnacLog.trace("new MuseumEnvironment");
        objects = new ArrayList<CObject>();
        captures = new ArrayList<Capture>();
        halls = new ArrayList<Hall>();
        links = new ArrayList<Link>();
        routes = new ArrayList<Route>();
        conns = new Hashtable<String,String>();
        props = new Properties();
        strings = new Hashtable<String, Hashtable<String,
                Hashtable<String, String> > >();
    }

    public MuseumEnvironment (String title)
    {
        this();
        BnacLog.trace("new MuseumEnvironment %s", title);
        set("Title", title);
    }

    public String getTitle () { return get("Title"); }

    public CObject[] objectArray ()
    {
        return objects.toArray(new CObject[0]);
    }

    public void addObject (CObject o)
    {
        BnacLog.debug("new object: %s, %s (%s)",
                o.get(Attribute.Author), o.get(Attribute.Title), o.getId());
        objects.add(o);
    }

    public CObject getObject (String id)
    {
        for (CObject obj: objects) {
            if (obj.get(Attribute.Id).equals(id)) {
                return obj;
            }
        }
        return null;
    }

    public int objectNo () { return objects.size(); }

    public Capture[] captureArray ()
    {
        return captures.toArray(new Capture[0]);
    }

    public void addCapture (Capture c)
    {
        BnacLog.debug("new capture: %s ref %s",
                c.getId(), c.getReferencedObject().getId());
        captures.add(c);
    }

    public Capture getCapture (String id)
    {
        for (Capture cap: captures) {
            if (cap.get(Attribute.Id).equals(id)) {
                return cap;
            }
        }
        return null;
    }

    public int captureNo () { return captures.size(); }

    public Hall[] hallArray ()
    {
        return halls.toArray(new Hall[0]);
    }
    public void addHall (Hall h) { halls.add(h); }
    public Hall getHall (String id)
    {
        for (Hall hall: halls) {
            if (hall.get(Attribute.Id).equals(id)) {
                return hall;
            }
        }
        return null;
    }
    public Hall removeHall (String id)
    {
        for (Hall hall: halls) {
            if (hall.get(Attribute.Id).equals(id)) {
                halls.remove(hall);
                return hall;
            }
        }
        return null;
    }
    
    public int hallNo () { return halls.size(); }

    public Hall getDoorParentHall (String id)
    {
        for (Hall h: halls) {
            if (h.getDoor(id) != null) { return h; }
        }
        return null;
    }

    public void addLink (Link l)
    {
        BnacLog.debug("add link %s", l.getId());
        links.add(l);
        conns.put(l.d1, l.d2);
        conns.put(l.d2, l.d1);
    }

    public Link removeLink (String id)
    {
        for (Link l: links) {
            if (l.getId().equals(id)) {
                BnacLog.debug("remove link %s", l.getId());
                conns.remove(l.d1);
                conns.remove(l.d2);
                links.remove(l);
                return l;
            }
        }
        return null;
    }

    public Link[] linkArray () { return links.toArray(new Link[0]); }

    public String getConnection (String id) { return conns.get(id); }

    public void addRoute (Route r)
    {
        BnacLog.debug("add route %s", r.getId());
        routes.add(r);
    }

    public Route removeRoute (String id)
    {
        for (int i=0; i<routes.size(); ++i) {
            if (routes.get(i).getId().equals(id)) {
                BnacLog.debug("remove route %s", id);
                return routes.remove(i);
            }
        }
        BnacLog.warn("could not find and remove route %s", id);
        return null;
    }

    public Route[] routeArray () { return routes.toArray(new Route[0]); }

    public String get(String name)
    {
        return props.getProperty(name);
    }
    public void set(String name, String val)
    {
        props.setProperty(name, val);
    }

    static public MuseumEnvironment fromFile (File file)
        throws IOException, SAXException, ParserConfigurationException
    {
        MuseumEnvironment me = new MuseumEnvironment();
        SAXParserFactory spfactory = SAXParserFactory.newInstance();
        SAXParser parser = spfactory.newSAXParser();
        MemlParser mp = new MemlParser(me);
        parser.parse(file, mp);

        return me;
    }

    public void addString (String id, String lang, String key, String val)
    {
        if (strings.get(id) == null) {
            strings.put(id, new Hashtable<String,
                    Hashtable<String, String> >());
        }
        if (strings.get(id).get(lang) == null) {
            strings.get(id).put(lang, new Hashtable<String, String>());
        }
        BnacLog.debug("adding [%s][%s][%s] = %s", id, lang, key, val);

        strings.get(id).get(lang).put(key, val);
    }

    public String getString (String id, String lang, String key)
    {
        if (strings.get(id) == null) {
            return null;
        }
        if (strings.get(id).get(lang) == null) {
            return null;
        }

        return strings.get(id).get(lang).get(key);
    }

    public Hashtable<String, Hashtable<String, Hashtable<String, String> > >
        getStrings ()
    {
        return strings;
    }
}

