/*
 *  bnac : virtual museum environment creation/manipulation project
 *  Copyright (C) 2010 Felipe Cañas Sabat
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cl.uchile.dcc.bnac;

import java.util.ArrayList;

public class Route
{
    protected String id;
    protected ArrayList<String> caps;

    public Route (String id)
    {
        this.id = id;
        caps = new ArrayList<String>();
    }

    public String getId () { return id; }

    public void addCapture (String id)
    {
        BnacLog.debug("add to route %s capture %s", this.id, id);
        caps.add(id);
    }

    public void addCapture (int idx, String id)
    {
        BnacLog.debug("add to route %s capture %s at %d", this.id, id, idx);
        caps.add(idx, id);
    }

    public void removeCapture (String id)
    {
        for (String c: caps) {
            if (c.equals(id)) {
                BnacLog.debug("remove from route %s capture %s",
                        this.id, id);
                caps.remove(c);
                return;
            }
        }
        BnacLog.warn("in route %s could not find and remove capture %s",
                this.id, id);
    }

    public void moveCapture (int idx, String id)
    {
        String s;
        for (int i=0; i<caps.size(); ++i) {
            if (caps.get(i).equals(id)) {
                BnacLog.debug("in route %s moved capture %s to %d",
                        this.id, id, idx);
                s = caps.remove(i);
                caps.add(idx, s);
                return;
            }
        }
        BnacLog.warn("in route %s could not find and move capture %s",
                this.id, id);
    }

    public String[] captureArray () { return caps.toArray(new String[0]); }

    public int captureNo () { return caps.size(); }
}

